import jwt
from fastapi import HTTPException,status,Header
from app.core.settings import secret_key
from datetime import datetime
from app.users.models import UserProfile


async def verify_user_identity(token: str = Header(...)):
    try:
        payload = jwt.decode(token, secret_key, algorithm="HS256")
        if datetime.fromtimestamp(payload["exp"]) < datetime.now():
            raise HTTPException(
                status_code = status.HTTP_401_UNAUTHORIZED,
                detail="Token expired",
                headers={"WWW-Authenticate": "Bearer"},
            )
        user_data = UserProfile.objects.get(id=payload["user"])
        return user_data
    except Exception as e:
        print(e)
        raise HTTPException(
            status_code=status.HTTP_403_FORBIDDEN,
            detail="Could not validate credentials",
            headers={"WWW-Authenticate": "Bearer"},
        )
        
 