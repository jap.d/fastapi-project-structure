from jsonschema import validate,ValidationError,SchemaError

user_schema = {
    "type":"object",
    "properties":{
        "first_name":{
            "type":"string"
        },
        "last_name":{
            "type":"string"
        },
        "email_id":{
            "type":"string",
            "format":"email"
        },
        "password":{
            "type":"string"
        }
    },
    "required":["first_name","last_name","email_id","password"],
    "additionalProperties":False
}


def validate_user_schema(data):
    try:
        validate(instance=data,schema=user_schema)
        return {"result":True,"data":data}
    except ValidationError as e:
        return {"result":False,"data":str(e)}
    except SchemaError as e:
        return {"result":False,"data":str(e)}

login_schema = {
    "type":"object",
    "properties":{
        "email_id":{    
            "type":"string",
            "format":"email"
        },
        "password":{
            "type":"string"
        }
    },
    "required":["email_id","password"],
    "additionalProperties":False
}


def validate_login_schema(data):
    try:
        validate(instance=data,schema=login_schema)
        return {"result":True,"data":data}
    except ValidationError as e:
        return {"result":False,"data":str(e)}
    except SchemaError as e:
        return {"result":False,"data":str(e)}   