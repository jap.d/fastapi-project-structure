import jwt
from app.core.settings import secret_key
from datetime import timezone,datetime,timedelta
from fastapi import HTTPException

def get_token(user_id):
    payload = {'user':user_id,'exp':datetime.now(tz=timezone.utc) + timedelta(seconds=600)}
    encoded_jwt = jwt.encode(payload, secret_key, algorithm="HS256")
    return str(encoded_jwt,"UTF-8")

