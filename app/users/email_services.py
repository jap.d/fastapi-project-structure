from fastapi_mail import MessageSchema,FastMail
from app.core.settings import conf

async def send_verification_email():
    try:
        message = MessageSchema(
            subject="Hello",
            recipients=["jap@yopmail.com"],
            body="hello",
            subtype='html',
        )
        
        fm = FastMail(conf)
        await fm.send_message(message, template_name='email.html')
        return
   
    except Exception as e:
        print(e)