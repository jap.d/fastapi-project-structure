from fastapi_mail import MessageSchema,FastMail
from app.core.settings import conf

async def send_emails():
    try:
        for i in range(0,11):
            message = MessageSchema(
                subject="Hello",
                recipients=["jap@yopmail.com"],
                body="hello",
                subtype='html',
            )
            fm = FastMail(conf)
            await fm.send_message(message, template_name='email.html')
            
    except Exception as e:
        print(e)