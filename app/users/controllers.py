from fastapi import APIRouter,Body,status,Depends,BackgroundTasks
from mongoengine.errors import ValidationError
from app.users.validations import *
from app.users.models import UserProfile
from app.users.schemas import *
from fastapi.responses import JSONResponse
from app.users.helpers import *
from app.users.authentication import verify_user_identity
from app.users.email_services import send_verification_email
from app.core.logger import get_logger
from fastapi_cachette import Cachette
from app.users.tasks import send_emails

router = APIRouter()
local_logger = get_logger(__name__)


@router.post('/signUp')
async def register_user(data: dict=Body(...)):
    try:
        response  = validate_user_schema(data)
        if not response["result"]:
            return response
        user_data = UserProfile(**response["data"])
        user_data.get_password(response["data"]["password"])
        user_schema = Userschema()
        resulted_data = user_schema.dump(user_data)
        await send_verification_email()
        response = {"result":True,"data":resulted_data}
        local_logger.info(f"{user_data.email_id} is registered as new user")
        return JSONResponse(status_code=status.HTTP_201_CREATED,content=response)
    
    except ValidationError as e:
        response = {"result":True,"data":str(e)}
        local_logger.error(str(e))
        return JSONResponse(status_code=status.HTTP_400_BAD_REQUEST,content=response)

@router.post('/signIn')
async def verify_login(data: dict=(Body(...))):
    try:
        response = validate_login_schema(data)
        if not response["result"]:
            local_logger.error(response["data"])
            return response
        try:
            user_data = UserProfile.objects.get(email_id=response["data"]["email_id"])
        except Exception as e:
            response = {"result":False,"data":"User with this email not found"}
            local_logger.error(str(e))
            return JSONResponse(status_code=status.HTTP_400_BAD_REQUEST,content=response)
        
        if user_data.verify_password(response["data"]["password"],user_data.password):
            try:
                jwt_token =  get_token(str(user_data.id))
                response = {"token":jwt_token,"user":user_data.email_id}
                local_logger.info(f"{user_data.email_id} is logged in the system")
                return JSONResponse(status_code=status.HTTP_200_OK,content=response)
            except Exception as e:
                response = {"result":False,"data":str(e)}
                local_logger.error(str(e))
                return JSONResponse(status_code=status.HTTP_400_BAD_REQUEST,content=response)

    except ValidationError as e:
        response = {"result":True,"data":str(e)}
        local_logger.error(str(e))
        return JSONResponse(status_code=status.HTTP_400_BAD_REQUEST,content=response)

@router.get('/userProfile')
async def get_user_profile(user=Depends(verify_user_identity),cache: Cachette = Depends()):
    response = {"result":True,"data":user.email_id}
    response if await cache.fetch("user_data") else await cache.put("user_data",response)

    local_logger.info(f"{user.email_id} has requested for profile view")
    return JSONResponse(status_code=status.HTTP_200_OK,content=response)

@router.get('/triggeremails')
async def trigger_emails(background_tasks:BackgroundTasks,user=Depends(verify_user_identity)):
    background_tasks.add_task(send_emails)
    local_logger.info(f"{user.email_id} has requested for profile view")
    return JSONResponse(status_code=status.HTTP_200_OK,content={"result":True})
