from mongoengine import *
from app.core.settings import pwd_context

class UserProfile(Document):

    meta = {
        'collection': 'UserProfile'
    }

    first_name = StringField(max_length=30,required=True)
    last_name = StringField(max_length=30,required=True)
    email_id = EmailField(required=True)
    password = StringField(max_length=100,required=True)

    def get_password(self,password):
        self.password = pwd_context.hash(password)
        super(UserProfile, self).save()

    def verify_password(self,password, hashed_password):
        return pwd_context.verify(password, hashed_password)
