from mongoengine import connect
from passlib.context import CryptContext
from fastapi_mail import ConnectionConfig
from pathlib import Path
import environ

env = environ.Env()
environ.Env.read_env()


db = connect(host=env("DB_HOST"))
secret_key = env("SECRET_KEY")
pwd_context = CryptContext(schemes=["bcrypt"], deprecated="auto")
conf = ConnectionConfig(
    MAIL_USERNAME=env("MAIL_USERNAME"),
    MAIL_PASSWORD=env("MAIL_PASSWORD"),
    MAIL_FROM=env("MAIL_FROM"),
    MAIL_PORT=env("MAIL_PORT"),
    MAIL_SERVER=env("MAIL_SERVER"),
    MAIL_FROM_NAME=env("MAIL_FROM_NAME"),
    MAIL_STARTTLS=env("MAIL_STARTTLS"),
    MAIL_SSL_TLS=env("MAIL_SSL_TLS"),
    USE_CREDENTIALS=env("USE_CREDENTIALS"),
    TEMPLATE_FOLDER=Path(__file__).parent / 'templates'
)